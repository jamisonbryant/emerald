Instructions for using Silk Icons:

    1. Leave Silk Icons main ZIP file in this directory
    2. When an icon is needed, drag it form the ZIP file to this directory
    3. Do NOT extract the entire ZIP file to this directory...it is unneccessary!
    
The following link must also be included somewhere in the application to credit the author:

    http://www.famfamfam.com/lab/icons/silk/