﻿<Application x:Class="Emerald.App"
             xmlns="http://schemas.microsoft.com/winfx/2006/xaml/presentation"
             xmlns:x="http://schemas.microsoft.com/winfx/2006/xaml"
             xmlns:local="clr-namespace:Emerald"
             Startup="Initialize">
    <Application.Resources>
        <!-- Title -->
        <Style TargetType="{x:Type TextBlock}" x:Key="Title">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontWeight" Value="Bold" />
            <Setter Property="FontSize" Value="28" />
            <Setter Property="Margin" Value="10" />
        </Style>

        <!-- Level 1 heading -->
        <Style TargetType="{x:Type TextBlock}" x:Key="Lv1Heading">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontWeight" Value="Bold" />
            <Setter Property="FontSize" Value="20" />
            <Setter Property="Margin" Value="5,5,0,10" />
        </Style>

        <!-- Level 2 heading -->
        <Style TargetType="{x:Type TextBlock}" x:Key="Lv2Heading">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontWeight" Value="Bold" />
            <Setter Property="FontSize" Value="19" />
            <Setter Property="Margin" Value="5,5,0,10" />
        </Style>

        <!-- Level 3 heading -->
        <Style TargetType="{x:Type TextBlock}" x:Key="Lv3Heading">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontStyle" Value="Italic" />
            <Setter Property="FontSize" Value="15" />
            <Setter Property="Margin" Value="5,0,0,10" />
        </Style>

        <!-- Inline warning -->
        <Style TargetType="{x:Type TextBlock}" x:Key="InlineWarning">
            <Setter Property="FontWeight" Value="Bold" />
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="Foreground" Value="Orange" />
        </Style>

        <!-- Inline error -->
        <Style TargetType="{x:Type TextBlock}" x:Key="InlineError">
            <Setter Property="FontWeight" Value="Bold" />
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="Foreground" Value="Red" />
        </Style>

        <!-- Copyright -->
        <Style TargetType="{x:Type TextBlock}" x:Key="Copyright">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontStyle" Value="Italic" />
            <Setter Property="Foreground" Value="Gray" />
            <Setter Property="Margin" Value="10" />
            <Setter Property="HorizontalAlignment" Value="Center" />
        </Style>

        <!-- Text field label -->
        <Style TargetType="{x:Type Label}" x:Key="TextFieldLabel">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontSize" Value="13" />
            <Setter Property="FontStyle" Value="Italic" />
        </Style>

        <!-- Text field -->
        <Style TargetType="{x:Type TextBox}" x:Key="TextField">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontSize" Value="15" />
            <Setter Property="Margin" Value="3,0" />
            <Setter Property="Padding" Value="2" />
        </Style>

        <!-- Big text field -->
        <Style TargetType="{x:Type TextBox}" x:Key="BigTextField">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontSize" Value="18" />
            <Setter Property="Padding" Value="5" />
        </Style>


        <!-- Password field -->
        <Style TargetType="{x:Type PasswordBox}" x:Key="PasswordField">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontSize" Value="15" />
            <Setter Property="Margin" Value="5,0" />
            <Setter Property="Padding" Value="2" />
        </Style>

        <!-- Big (hi-vis) button -->
        <Style TargetType="{x:Type Button}" x:Key="BigButton">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontSize" Value="18" />
            <Setter Property="FontWeight" Value="Bold" />
            <Setter Property="Padding" Value="10,5" />
        </Style>

        <!-- Medium button -->
        <Style TargetType="{x:Type Button}" x:Key="MediumButton">
            <Setter Property="FontFamily" Value="Arial" />
            <Setter Property="FontSize" Value="16" />
            <Setter Property="FontWeight" Value="Bold" />
            <Setter Property="Padding" Value="10,7" />
        </Style>
    </Application.Resources>
</Application>
