﻿using Bogus;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    /// <summary>
    /// 
    /// </summary>
    public class InvalidResponse
    {
        public string Name { get; set; }
        public string PhoneNumber { get; set; }
        public string ResponseText { get; set; }
        public MilitaryTime Received { get; set; }

        /// <summary>
        /// Overwrites object data with fake data (for demoing/testing purposes)
        /// </summary>
        /// <returns>A fake invalid response</returns>
        public InvalidResponse Fake(Incident incident)
        {
            Faker faker = new Faker();
            Volunteer v = new Volunteer().Fake();
            List<string> invalidResponseExamples = new List<string> {
                "Who is this",
                "I am driving right now, I will text you back shortly.",
                "new phone who dis",
                "I can be there in 40 minutes, is that OK?",
                "I cannot respond",
                "Responding",
                "Ooh, how exciting!",
                "On my way",
                "en route",
                "What do I need to bring?"
            };

            Name = string.Format("{0} {1}", v.FirstName, v.LastName);
            PhoneNumber = v.PrimaryPhone;
            ResponseText = faker.PickRandom<string>(invalidResponseExamples);

            // Convert ETA in minutes to military time
            Received = incident.StartTime;
            var minutes = faker.Random.Int(0, 60 - incident.StartTime.Minutes);
            Received.Minutes = minutes;

            return this;
        }
    }
}
