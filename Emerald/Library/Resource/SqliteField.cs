﻿namespace Emerald
{
    public class SqliteField
    {
        /// <summary>
        /// Valid field types
        /// </summary>
        public enum FieldType { INTEGER, TEXT, BLOB, REAL, NUMERIC }
        
        /// <summary>
        /// Field name
        /// </summary>
        public string name;

        /// <summary>
        /// Field type
        /// </summary>
        public FieldType type;

        /// <summary>
        /// Can the field be null?
        /// </summary>
        public bool isNotNull;

        /// <summary>
        /// Does the field define a primary key?
        /// </summary>
        public bool isPrimaryKey;

        /// <summary>
        /// Does the field auto-increment?
        /// </summary>
        public bool isAutoIncrement;

        /// <summary>
        /// Is the field value required to be unique?
        /// </summary>
        public bool isUnique;

        /// <summary>
        /// Default value of the field
        /// </summary>
        public object defaultValue;

        /// <summary>
        /// Partial class constructor
        /// </summary>
        public SqliteField(string name, FieldType type)
        {
            this.name = name;
            this.type = type;
        }

        /// <summary>
        /// Partial class constructor
        /// </summary>
        public SqliteField(string name, FieldType type, bool isNotNull, bool isPrimaryKey, bool isAutoIncrement)
        {
            this.name = name;
            this.type = type;
            this.isNotNull = isNotNull;
            this.isPrimaryKey = isPrimaryKey;
            this.isAutoIncrement = isAutoIncrement;
        }        
        
        /// <summary>
        /// Complete class constructor
        /// </summary>
        public SqliteField(string name, FieldType type, bool isNotNull, bool isPrimaryKey, bool isAutoIncrement, bool isUnique, object defaultValue)
        {
            this.name = name;
            this.type = type;
            this.isNotNull = isNotNull;
            this.isPrimaryKey = isPrimaryKey;
            this.isAutoIncrement = isAutoIncrement;
            this.isUnique = isUnique;
            this.defaultValue = defaultValue;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string ToSqlString()
        {
            // Identify string representation of field type from field
            // type class property (too bad we can't convert an enum entry
            // directly to a string...)
            string fieldType = "";
            switch (type) {
                case FieldType.BLOB:
                    fieldType = "BLOB";
                    break;

                case FieldType.INTEGER:
                    fieldType = "INTEGER";
                    break;

                case FieldType.NUMERIC:
                    fieldType = "NUMERIC";
                    break;

                case FieldType.REAL:
                    fieldType = "REAL";
                    break;

                case FieldType.TEXT:
                    fieldType = "TEXT";
                    break;
            }

            // Identify type of default value property and construct SQLite snippet
            // to define the equivalent table property. For example, if the default
            // value is a string use DEFAULT "foobar", whereas if it is an int, use
            // DEFAULT 1234.

            string fieldDefaultValue = "";

            if (defaultValue != null) {
                fieldDefaultValue += "DEFAULT ";

                if (defaultValue is string) {
                    fieldDefaultValue += string.Format("'{0}'", defaultValue);
                } else {
                    fieldDefaultValue += defaultValue;
                }
            }

            /// Return the formatted string
            return string.Format("`{0}` {1} {2} {3} {4} {5} {6}",
                name, 
                fieldType, 
                (isNotNull ? "NOT NULL" : ""),
                (isPrimaryKey ? "PRIMARY KEY" : ""),
                (isAutoIncrement ? "AUTOINCREMENT" : ""),
                (isUnique ? "UNIQUE" : ""),
                fieldDefaultValue
            );
        }
    }
}