﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Diagnostics;
using System.Windows.Navigation;
using System.Net;
using Emerald.Library.View;

namespace Emerald
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window
    {
        public LoginWindow()
        {
           // Initialize window
            InitializeComponent();

            // Test Internet connection
            // If not OK, display Internet connection warning
            if (!InternetConnectionOk()) {
                InternetConnectionWarning.Visibility = Visibility.Visible;
            }

            // Test CERVIS connection
            // If not OK, display CERVIS connection error
            if (!CervisConnectionOk()) {
                CervisConnectionError.Visibility = Visibility.Visible;
            }
         }

        /// <summary>
        /// Tests if the application can communicate with CERVIS.
        /// </summary>
        /// <returns>True if connection successful, false otherwise.</returns>
        private bool CervisConnectionOk()
        {
            CervisComponent c = new CervisComponent();
            return c.CanConnectToCervis();
        }

        /// <summary>
        /// Tests if the application can connect to the Internet.
        /// </summary>
        /// <returns>True if connection successful, false otherwise</returns>
        private bool InternetConnectionOk()
        {
            return NetHelper.CanConnectToInternet();
        }

        /// <summary>
        /// Opens the Montgomery CERT website in the default browser.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void NavigateToUri(object sender, RequestNavigateEventArgs e)
        {
            System.Diagnostics.Process.Start(e.Uri.ToString());
        }

        /// <summary>
        /// Processes and submits the login form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SubmitLoginForm(object sender, RoutedEventArgs e)
        {
            Button b = (Button) sender;
            string username = UsernameField.Text;
            string password = PasswordField.Password;

            // Change button appearance to indicate form is working
            b.IsEnabled = false;

            // Check if form is valid
            // If so, continue to authentication
            if (ValidateForm()) {
                // Do authentication
                AuthComponent a = new AuthComponent();

                if (a.AuthenticateUser(username, password)) {
                    //Volunteer user = (Volunteer) Application.Current.Properties["User"];
                    //MessageBox.Show(String.Format("Logged in as {0} {1} ({2})", user.FirstName, user.LastName, user.Email));

                    // Open primary action window and close current window
                    (new ActionWindow()).Show();
                    Close();
                } else {
                    // Display authentication failure error message
                    MessageBox.Show("Invalid username or password. Please try again.");
                }
            }

            // Revert button appearance
            b.IsEnabled = true;
        }

        /// <summary>
        /// Validates the login form.
        /// </summary>
        /// <returns>True if valid, false otherwise.</returns>
        private bool ValidateForm()
        {
            bool valid = true;
            string username = UsernameField.Text;
            string password = PasswordField.Password;

            // Check if username is blank
            // If so, set invalid switch to true and update field style
            if (string.IsNullOrEmpty(username)) {
                valid = false;
                FormHelper.MarkRequiredFieldInvalid(UsernameField);
           }

            // Check if password is blank
            // If so, set invalid switch to true and update field style
            if (string.IsNullOrEmpty(password)) {
                valid = false;
                FormHelper.MarkRequiredFieldInvalid(PasswordField);
            }

            // Display validation failure warning
            if (!valid) {
                MessageBox.Show("One or more required fields are missing. Please check your inputs and try again.");
            }

            return valid;
        }
    }
}
