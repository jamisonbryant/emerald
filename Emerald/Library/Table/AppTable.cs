﻿using Microsoft.Win32.SafeHandles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public abstract class AppTable : IDisposable
    {
        /// <summary>
        /// Table name
        /// </summary>
        protected string name;

        /// <summary>
        /// List of table  fields
        /// </summary>
        protected List<SqliteField> fields = new List<SqliteField>();
        
        /// <summary>
        /// Has Dispose() already been called?
        /// </summary>
        bool isDisposed = false;

        /// <summary>
        /// SafeHandle instance
        /// </summary>
        SafeHandle handle = new SafeFileHandle(IntPtr.Zero, true);

        /// <summary>
        /// Public implementation of Dispose pattern callable by consumers 
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Protected implementation of Dispose pattern
        /// </summary>
        /// <param name="disposing"></param>
        protected virtual void Dispose(bool disposing)
        {
            if (isDisposed)
                return;

            if (disposing) {
                handle.Dispose();

                // Free any other managed objects here.
            }

            // Free any unmanaged objects here.
            isDisposed = true;
        }

        /// <summary>
        /// Returns the table sqlinition as a SQL statement
        /// </summary>
        /// <returns>SQL sqlinition of the table</returns>
        public string ToSqlString()
        {
            string sql = "";

            if (!string.IsNullOrEmpty(name)) {
                sql = string.Format("CREATE TABLE `{0}` ", name);

                // Add fields to table sqlinition, if any
                if (fields.Count() != 0) {
                    List<string> fieldSql = new List<string>();

                    foreach (var field in fields) {
                        fieldSql.Add(field.ToSqlString().TrimEnd());
                    }

                    sql += string.Format("({0});", string.Join(", ", fieldSql));
                }
            }

            return sql;
        }
    }
}
