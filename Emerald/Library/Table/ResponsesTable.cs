﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public class ResponsesTable : AppTable
    {
        public ResponsesTable()
        {
            // Set table name
            name = "responses";

            // Define table fields
            fields.Add(new SqliteField("id", SqliteField.FieldType.INTEGER, true, true, true));
            fields.Add(new SqliteField("response_text", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("receive_time", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("first_name", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("last_name", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("phone_number", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("email", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("eta", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("is_cdo", SqliteField.FieldType.INTEGER));
            fields.Add(new SqliteField("is_deputy", SqliteField.FieldType.INTEGER));
            fields.Add(new SqliteField("is_first_on_scene", SqliteField.FieldType.INTEGER));
            fields.Add(new SqliteField("volunteer_id", SqliteField.FieldType.INTEGER));
            fields.Add(new SqliteField("incident_id", SqliteField.FieldType.INTEGER));
        }
    }
}
