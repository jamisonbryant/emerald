﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public class VolunteersTable : AppTable
    {
        public VolunteersTable()
        {
            // Set table name
            name = "volunteers";

            // Define table fields
            fields.Add(new SqliteField("id", SqliteField.FieldType.INTEGER, true, true, true));
            fields.Add(new SqliteField("first_name", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("last_name", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("email", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("primary_phone", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("alternate_phone", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("street_address_1", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("street_address_2", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("city", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("state", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("zip_code", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("account_type", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("account_status", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("creation_date", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("last_active", SqliteField.FieldType.TEXT));
        }
    }
}
