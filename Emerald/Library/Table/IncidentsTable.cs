﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public class IncidentsTable : AppTable
    {
        public IncidentsTable()
        {
            // Set table name
            name = "incidents";

            // Define table fields
            fields.Add(new SqliteField("id", SqliteField.FieldType.INTEGER, true, true, true));
            fields.Add(new SqliteField("name", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("description", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("start_time", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("end_time", SqliteField.FieldType.TEXT));
        }
    }
}
