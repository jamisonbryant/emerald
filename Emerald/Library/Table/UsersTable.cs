﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public class UsersTable : AppTable
    {
        public UsersTable()
        {
            // Set table name
            name = "users";

            // Define table fields
            fields.Add(new SqliteField("id", SqliteField.FieldType.INTEGER, true, true, true));
            fields.Add(new SqliteField("first_name", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("last_name", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("email", SqliteField.FieldType.TEXT));
            fields.Add(new SqliteField("phone_number", SqliteField.FieldType.TEXT));
        }
    }
}
