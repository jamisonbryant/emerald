﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SQLite;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public class DatabaseComponent
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// SQLite version to use (should be 3)
        /// </summary>
        public static int SqliteVersion = 3;

        /// <summary>
        /// Name of file containign SQLite database 
        /// </summary>
        public static string DatabaseFileName = string.Format("{0}.sqlite", App.AppName); 

        /// <summary>
        /// Full path to SQLite database file 
        /// </summary>
        public static string DatabaseFilePath = string.Format("{0}{1}{2}.sqlite", App.DataDir, Path.DirectorySeparatorChar, App.AppName);

        /// <summary>
        /// Database connection string (used by SQLite library to connect to database) 
        /// </summary>
        public static string ConnectionString
        {
            get { return string.Format("Data Source={0};Version={1};", DatabaseFilePath, SqliteVersion); }
        }

        /// <summary>
        /// Creates the database *.sqlite file if it does not already exist.
        /// </summary>
        public void CreateDatabase()
        { 
            // Check if database file exists at expected path
            // If not, create it. If so, check its integrity.
            if (!File.Exists(DatabaseFilePath)) {
            // Create database *.sqlite file and connect to the database
                SQLiteConnection.CreateFile(DatabaseFilePath);
                SQLiteConnection conn = ConnectAndOpen();

                // Create SQLite commands for creating the various tables
                // Each command will be executed against the connection created above
                // to create the database structure.
                using (VolunteersTable t = new VolunteersTable()) {
                    string sql = t.ToSqlString();
                    SQLiteCommand c = new SQLiteCommand(sql, conn);
                    c.ExecuteNonQuery();
                }

                using (UsersTable t = new UsersTable()) {
                    string sql = t.ToSqlString();
                    SQLiteCommand c = new SQLiteCommand(sql, conn);
                    c.ExecuteNonQuery();
                }

                using (IncidentsTable t = new IncidentsTable()) {
                    string sql = t.ToSqlString();
                    SQLiteCommand c = new SQLiteCommand(sql, conn);
                    c.ExecuteNonQuery();
                }

                using (ResponsesTable t = new ResponsesTable()) {
                    string sql = t.ToSqlString();
                    SQLiteCommand c = new SQLiteCommand(sql, conn);
                    c.ExecuteNonQuery();
                }
            } else {
                CheckIntegrity();
            }
       }

        /// <summary>
        /// Checks database integrity. Throws an exception if existing schema does not match expected schema.
        /// </summary>
        private void CheckIntegrity()
        {
            
            //throw new NotImplementedException();
        }

        /// <summary>
        /// Creates a connection to the database for querying/inserting data.
        /// </summary>
        /// <remarks>Connection is NOT OPENED automatically. `Open()` must be called before queries are issued.</remarks>
        /// <see cref="ConnectAndOpen" />
        /// <returns>An unopened connection to the database</returns>
        public SQLiteConnection Connect()
        {
            return new SQLiteConnection(ConnectionString);
        }

        /// <summary>
        /// Creates a connection to the database for querying/inserting data.
        /// </summary>
        /// <remarks>Connection is OPENED automatically. There is no need to call `Open()`</remarks>
        /// <see cref="Connect" />
        /// <returns>An open connection to the database</returns>
        public SQLiteConnection ConnectAndOpen()
        {
            SQLiteConnection conn = new SQLiteConnection(ConnectionString);
            conn.Open();

            return conn;
        }
    }
}
