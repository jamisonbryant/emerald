﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Xml;
using System.Globalization;

namespace Emerald
{
    /// <summary>
    /// 
    /// </summary>
    public class ConfigComponent
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// Log file
        /// </summary>
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static bool HasSetting(string key)
        {
            try {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;
                return !(settings[key] == null);
            } catch (ConfigurationErrorsException) {
                throw;
            }
        }

        public static void AddSetting(string key, string value, bool overwrite = false)
        {
            try {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;

                if (settings[key] == null) {
                    settings.Add(key, value);
                } else {
                    if (overwrite) {
                        var oldValue = settings[key].Value;
                        log.Warn(string.Format("Overwriting setting {0} (value: {1}) with new value: {2}", key, oldValue, value));
                        settings[key].Value = value;
                    }
                }

                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            } catch (ConfigurationErrorsException) {
                throw;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <returns></returns>
        public static string GetSetting(string key)
        {
            var setting = "";

            if (HasSetting(key)) {
                try {
                    var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                    var settings = configFile.AppSettings.Settings;
                    setting = settings[key].Value.ToString();
                } catch (ConfigurationErrorsException) {
                    throw;
                }
            } else {
                log.Error(string.Format("Requested configuration setting not found: {0}", key));
            }

            return setting;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static Dictionary<string, string> GetAllSettings()
        {
            Dictionary<string, string> settings = new Dictionary<string, string>();

            try {
                var appSettings = ConfigurationManager.AppSettings;

                foreach (var key in appSettings.AllKeys) {
                    settings.Add(key, appSettings[key]);
                }
            } catch (ConfigurationErrorsException) {
                Console.WriteLine("Error reading app settings");
            }

            return settings;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="key"></param>
        /// <param name="value"></param>
        /// <param name="create"></param>
        public static void UpdateSetting(string key, string value, bool create = false)
        {
            try {
                var configFile = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
                var settings = configFile.AppSettings.Settings;

                if (settings[key] != null) {
                    settings[key].Value = value;
                } else {
                    if (create) {
                        settings.Add(key, value);
                    }
                }

                configFile.Save(ConfigurationSaveMode.Modified);
                ConfigurationManager.RefreshSection(configFile.AppSettings.SectionInformation.Name);
            } catch (ConfigurationErrorsException) {
                throw;
            }
        }
    }
}