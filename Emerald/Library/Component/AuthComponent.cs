﻿using System;
using System.Configuration;
using System.Security.Cryptography;

namespace Emerald
{
    public class AuthComponent
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// Log file
        /// </summary>
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Authenticates user against the CERVIS responder database.
        /// </summary>
        /// <param name="username">Username of user to authenticate</param>
        /// <param name="password">Password of user to authenticate</param>
        /// <returns></returns>
        public bool AuthenticateUser(string username, string password)
        {
            bool isAuthed = false;
            Volunteer vol = new Volunteer();

            // == If Demo Mode is enabled, authenticate a fake user
            if (demoMode)
            {
                vol = vol.Fake();
                System.Windows.Application.Current.Properties["User"] = vol;
                return true;
            }
            // == END DEMO MODE CODE

            CervisComponent c = new CervisComponent();

            if (c.CanConnectToCervis()) {
                vol = c.GetVolunteerFromEmail(username);

                if (!vol.ToString().Equals("")) {
                    // FIXME: Remove in prod!!
                    isAuthed = true;
                    //isAuthed = vol.GetPassword().Equals(password);
                }
            } else {
                isAuthed = false;
            }

            // If user authenticates successfully, store their info in the session
            // in case other processes need to access it.
            if (isAuthed && vol != null) {
                System.Windows.Application.Current.Properties["User"] = vol;
            }

            return isAuthed;
        }
    }
}