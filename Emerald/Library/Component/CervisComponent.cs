﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;
using System.Net;
using System.Xml;
using System.Globalization;
using Bogus;

namespace Emerald
{
    public class CervisComponent
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");
        
        /// <summary>
        /// Log file
        /// </summary>
        private static readonly log4net.ILog log =
            log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        /// <summary>
        /// Organization ID ("org ID") of CERVIS organization (e.g. 1234).
        /// </summary>
        private string orgId;

        /// <summary>
        /// CERVIS API endpoint URL
        /// </summary>
        private string apiUrl;

        /// <summary>
        /// Username of user accessing CERVIS API.
        /// </summary>
        private string apiUsername;

        /// <summary>
        /// Plain-text password of user accessing CERVIS API.
        /// </summary>
        private string apiPassword;

        /// <summary>
        /// The last response (as XML) from the CERVIS API.
        /// </summary>
        private string lastResponse;

        /// <summary>
        /// Loads API connection parameters and prepares component for use. 
        /// </summary>
        public CervisComponent()
        {
            // Capture API connection parameters from the config file.
            orgId = ConfigurationManager.AppSettings["CervisApiOrgId"];
            apiUrl = ConfigurationManager.AppSettings["CervisApiUrl"];
            apiUsername = ConfigurationManager.AppSettings["CervisApiUsername"];
            apiPassword = ConfigurationManager.AppSettings["CervisApiPassword"];
        }

        /// <summary>
        /// Checks if the component can connect to CERVIS.
        /// </summary>
        /// <returns>Returns true if connection is successful, false otherwise.</returns>
        public bool CanConnectToCervis()
        {
            // == If Demo Mode is enabled, short-circuit this check
            if (demoMode) { return true; }
            // == END DEMO MODE CODE

            bool canConnect = false;

            // First check if the app can connect to the Internet
            // If it can't, it can't connect to CERVIS either, so return false.
            if (NetHelper.CanConnectToInternet()) {
                // Submit basic request to CERVIS API
                // API should respond with code 200 (OK)
                using (WebClient client = new WebClient())
                {
                    byte[] response = client.UploadValues(apiUrl, new System.Collections.Specialized.NameValueCollection()
                    {
                        { "org_id", orgId },
                        { "login_email", apiUsername },
                        { "login_pw", apiPassword },
                        { "lookup_type", "vol" },
                        { "lookup_method", "id" },
                        { "result_type", "id" },
                        { "action", "select" }
                    });

                    // Parse response from byte array into XML
                    // Then get status code node value from XML via xpath
                    lastResponse = System.Text.Encoding.UTF8.GetString(response);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(lastResponse);
                    var status = xmlDoc.SelectSingleNode("/response/status/code").InnerText;

                    // If the status code is 200, connection was successful.
                    // Otherwise, read the error message from the response and log it.
                    if (status.Equals("200")) {
                        canConnect = true;
                    } else {
                        var message = xmlDoc.SelectSingleNode("/response/status/message");
                        log.Error("Unable to connect to CERVIS: " + message.InnerText);
                    }
                }
            } else {
                canConnect = false;
            }

            return canConnect;
        }

        /// <summary>
        /// Looks up a volunteer in the CERVIS database by email address.
        /// </summary>
        /// <param name="email">Email of volunteer to look up</param>
        /// <returns>Volunteer object if volunteer email found</returns>
        public Volunteer GetVolunteerFromEmail(string email)
        {
            Volunteer vol = null;

            // If Demo Mode is enabled, return a fake volunteer
            if (demoMode) { return vol.Fake(email); }
            // == END DEMO MODE CODE

            if (CanConnectToCervis()) {
                // Submit lookup request to CERVIS API
                using (WebClient client = new WebClient())
                {
                    byte[] response = client.UploadValues(apiUrl, new System.Collections.Specialized.NameValueCollection()
                    {
                        { "org_id", orgId },
                        { "login_email", apiUsername },
                        { "login_pw", apiPassword },
                        { "lookup_type", "vol" },
                        { "lookup_method", "email" },
                        { "result_type", "full" },
                        { "action", "select" },
                        { "lookup_vol_email", email }
                    });

                    // Parse response from byte array into XML
                    // Then get status code node value from XML via xpath
                    lastResponse = System.Text.Encoding.UTF8.GetString(response);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(lastResponse);

                    int responseCode = 0;
                    int numRecords = 0;

                    if (xmlDoc.HasChildNodes) {
                        responseCode = int.Parse(xmlDoc.SelectSingleNode("//response/status/code").InnerText);
                        numRecords = int.Parse(xmlDoc.SelectSingleNode("//response/status/records").InnerText);
                    }

                    // Check if CERVIS API returned exactly one record
                    // If so, create and populate the volunteer object...otherwise, vol is null.
                    if (responseCode == 200 && numRecords == 1) {
                        // Set volunteer properties from XML data
                        // One property (email) is pulled from the method parameters.
                        vol = new Volunteer()
                        {
                            Id = int.Parse(xmlDoc.SelectSingleNode("//volunteer/cervis_vol_id").InnerText),
                            FirstName = xmlDoc.SelectSingleNode("//volunteer/fname").InnerText,
                            LastName = xmlDoc.SelectSingleNode("//volunteer/lname").InnerText,
                            Email = email,
                            PrimaryPhone = xmlDoc.SelectSingleNode("//volunteer/priphone").InnerText,
                            AlternatePhone = xmlDoc.SelectSingleNode("//volunteer/altphone").InnerText,
                            StreetAddress1 = xmlDoc.SelectSingleNode("//volunteer/address").InnerText,
                            StreetAddress2 = xmlDoc.SelectSingleNode("//volunteer/address_line_2").InnerText,
                            City = xmlDoc.SelectSingleNode("//volunteer/city").InnerText,
                            State = xmlDoc.SelectSingleNode("//volunteer/state").InnerText,
                            ZipCode = xmlDoc.SelectSingleNode("//volunteer/zip").InnerText,
                            AccountType = xmlDoc.SelectSingleNode("//volunteer/acct_permission").InnerText,
                            AccountStatus = xmlDoc.SelectSingleNode("//volunteer/acct_status").InnerText,
                            CreationDate = DateTime.ParseExact(xmlDoc.SelectSingleNode("//volunteer/cervis_start_date").InnerText,
                            "yyyy-MM-dd", CultureInfo.InvariantCulture),
                            LastActive = DateTime.ParseExact(xmlDoc.SelectSingleNode("//volunteer/last_activity").InnerText,
                            "yyyy-MM-dd", CultureInfo.InvariantCulture)
                        };
                    }
                    else {
                        log.Error("CERVIS volunteer lookup failed for " + email);
                    }
               }
            }

            return vol;
        }

        public List<Volunteer> GetVolunteersInGroup(string groupName)
        {
            List<Volunteer> groupMembers = new List<Volunteer>();

            // If Demo Mode is enabled, return a list of fake volunteers
            if (demoMode) {
                for (var i = 0; i < 10; i++)
                {
                    Volunteer vol = new Volunteer();
                    groupMembers.Add(vol.Fake());
                }

                return groupMembers;
            }
            // == END DEMO MODE CODE

            if (CanConnectToCervis())
            {
                // Submit lookup request to CERVIS API
                using (WebClient client = new WebClient())
                {
                    byte[] response = client.UploadValues(apiUrl, new System.Collections.Specialized.NameValueCollection()
                    {
                        { "org_id", orgId },
                        { "login_email", apiUsername },
                        { "login_pw", apiPassword },
                        { "lookup_type", "vol" },
                        { "lookup_method", "all" },
                        { "result_type", "full" },
                        { "action", "select" },
                        { "vol_inc_attr", "yes" }
                    });

                    // Parse response from byte array into XML
                    // Then get status code node value from XML via xpath
                    lastResponse = System.Text.Encoding.UTF8.GetString(response);

                    XmlDocument xmlDoc = new XmlDocument();
                    xmlDoc.LoadXml(lastResponse);

                    int responseCode = 0;
                    int numRecords = 0;

                    if (xmlDoc.HasChildNodes)
                    {
                        responseCode = int.Parse(xmlDoc.SelectSingleNode("//response/status/code").InnerText);
                        numRecords = int.Parse(xmlDoc.SelectSingleNode("//response/status/records").InnerText);
                    }

                    if (responseCode == 200)
                    {
                        foreach (XmlNode node in xmlDoc.SelectNodes("//volunteer"))
                        {
                            string id = node.SelectSingleNode("//volunteer/cervis_vol_id").InnerText;

                            byte[] response2 = client.UploadValues(apiUrl, new System.Collections.Specialized.NameValueCollection()
                            {
                                { "org_id", orgId },
                                { "login_email", apiUsername },
                                { "login_pw", apiPassword },
                                { "lookup_type", "vol" },
                                { "lookup_method", "id" },
                                { "result_type", "all" },
                                { "action", "select" },
                                { "vol_inc_attr", "yes" },
                                { "lookup_vol_id", id }
                            });

                            // Parse response from byte array into XML
                            // Then get status code node value from XML via xpath
                            lastResponse = System.Text.Encoding.UTF8.GetString(response2);

                            XmlDocument xmlDoc2 = new XmlDocument();
                            xmlDoc2.LoadXml(lastResponse);

                            int responseCode2 = 0;
                            int numRecords2 = 0;

                            if (xmlDoc2.HasChildNodes) {
                                responseCode2 = int.Parse(xmlDoc2.SelectSingleNode("//response/status/code").InnerText);
                                numRecords2 = int.Parse(xmlDoc2.SelectSingleNode("//response/status/records").InnerText);
                            }

                            if (responseCode2 == 200 && numRecords2 == 1) {
                                foreach (XmlNode node2 in xmlDoc2.SelectNodes("//group")) {
                                    if (node2.SelectSingleNode("/group_name").InnerText.Equals(groupName)) {
                                        // Add volunteer to group
                                        groupMembers.Add(new Volunteer() {
                                            FirstName = node.SelectSingleNode("//volunteer/fname").InnerText,
                                            LastName = node.SelectSingleNode("//volunteer/lname").InnerText,
                                            PrimaryPhone = xmlDoc.SelectSingleNode("//volunteer/priphone").InnerText,
                                        });

                                        break;
                                    }
                                }
                            } else {
                                log.Error(string.Format("Unexpected number of results from CERVIS API when doing CDO lookup " +
                                    "(got {0}, expected 1)", numRecords2));
                            }
                        }
                    } else {
                        log.Error("CERVIS CDO lookup failed");
                    }
                }

            }

            return groupMembers;
        }
    }
}
