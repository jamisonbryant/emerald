﻿using Bogus;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    /// <summary>
    /// A POD class that represents a CERVIS volunteer.
    /// </summary>
    /// TODO: Sync up model with database table definition
    public class Volunteer : AppModel
    {
        /// <summary>
        /// Gender options
        /// </summary>
        public enum GenderOptions
        {
            Male,
            Female
        }

        /// <summary>
        /// Database row ID
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Volunteer gender
        /// </summary>
        public GenderOptions Gender;

        /// <summary>
        /// Volunteer first name
        /// </summary>
        public string FirstName { get; set; }

        /// <summary>
        /// Volunteer last name
        /// </summary>
        public string LastName { get; set; }

        /// <summary>
        /// Volunteer email address
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Volunteer primary phone (555-555-5555)
        /// </summary>
        public string PrimaryPhone { get; set; }

        /// <summary>
        /// Volunteer alternate phone (555-555-5555)
        /// </summary>
        public string AlternatePhone { get; set; }

        /// <summary>
        /// Volunteer street address (123 Main St)
        /// </summary>
        public string StreetAddress1 { get; set; }

        /// <summary>
        /// Volunteer street address, line 2 (Apt 12)
        /// </summary>
        public string StreetAddress2 { get; set; }

        /// <summary>
        /// Volunteer city (Boston)
        /// </summary>
        public string City { get; set; }

        /// <summary>
        /// Volunteer state (MA)
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Volunteer zip code (02134)
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Volunteer account type (Volunteer, Administrator...)
        /// </summary>
        public string AccountType { get; set; }

        /// <summary>
        /// Volunteer account status (Active, Inactive...)
        /// </summary>
        public string AccountStatus { get; set; }

        /// <summary>
        /// Date volunteer was created in CERVIS
        /// </summary>
        public DateTime CreationDate { get; set; }

        /// <summary>
        /// Date volunteer last accessed CERVIS
        /// </summary>
        public DateTime LastActive { get; set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public string GetPassword()
        {
            string pword = "";

            pword += FirstName.Substring(0, 1);
            pword += LastName;
            pword += Id.ToString();
            pword = pword.ToLower();

            return pword;
        }

        /// <summary>
        /// Overwrites object data with fake data (for demoing/testing purposes)
        /// </summary>
        /// <returns>A fake volunteer</returns>
        public Volunteer Fake(string email = null)
        {
            var faker = new Faker();

            Id = faker.Random.Number();
            Gender = faker.PickRandom<GenderOptions>();
            FirstName = faker.Name.FirstName((Bogus.DataSets.Name.Gender) Gender);
            LastName = faker.Name.LastName((Bogus.DataSets.Name.Gender) Gender);
            Email = email ?? faker.Internet.Email(FirstName, LastName);
            PrimaryPhone = faker.Phone.PhoneNumber();
            AlternatePhone = faker.Phone.PhoneNumber();
            StreetAddress1 = faker.Address.StreetAddress();
            StreetAddress2 = "";
            City = faker.Address.City();
            State = faker.Address.State();
            ZipCode = faker.Address.ZipCode();
            AccountStatus = "???";
            AccountType = "???";
            CreationDate = faker.Date.Past(5);
            LastActive = faker.Date.Past(1);

            return this;
        }

        /// <summary>
        /// Converts the volunteer object to a string
        /// </summary>
        public new string ToString()
        {
            return string.Format("{0} {1} ({2})", FirstName, LastName, PrimaryPhone);
        }
    }
}
