﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Emerald.Library.Model
{
    public class Dispatch : AppModel
    {
        /// <summary>
        /// Valid responses
        /// </summary>
        public ObservableCollection<ValidResponse> LoggedResponses { get; set; }

        /// <summary>
        /// Invalid responses (need processing)
        /// </summary>
        public ObservableCollection<InvalidResponse> InvalidResponses { get; set; }

        /// <summary>
        /// Volunteer doing the dispatching
        /// </summary>
        public Volunteer Dispatcher { get; set; }

        /// <summary>
        /// Dispatch incident
        /// </summary>
        public Incident Incident { get; set; }

        /// <summary>
        /// Is this dispatch a test?
        /// </summary>
        public bool IsATest { get; set; }
    }
}
