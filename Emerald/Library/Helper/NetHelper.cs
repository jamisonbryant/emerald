﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace Emerald
{
    public class NetHelper
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// Checks if the application can connect to the Internet.
        /// </summary>
        /// <returns>True if connection successful, false otherwise.</returns>
        /// <todo>Check backup URL(s) if primary unavailable (site may be down)</todo>
        public static bool CanConnectToInternet()
        {
            // == If Demo Mode is enabled, short-circuit this check
            //if (demoMode) { return true; }
            // == END DEMO MODE CODE

            try
            {
                using (var client = new WebClient()) {
                    using (var stream = client.OpenRead("http://www.google.com")) {
                        return true;
                    }
                 }
            } catch {
                return false;
            }
        }
    }
}
