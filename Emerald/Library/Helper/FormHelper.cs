﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Emerald
{
    public class FormHelper
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// Styles a form field with a light red background and a dark red border
        /// </summary>
        /// <param name="field">Field to style (can be any Control type, but should be either TextBox or PasswordBox)</param>
        public static void MarkRequiredFieldInvalid(Control field)
        {
            StyleField(field, Brushes.Red, Brushes.Pink);
        }

         /// <summary>
        /// Styles a form field with a light yellow background and a dark yellow border
        /// </summary>
        /// <param name="field">Field to style (can be any Control type, but should be either TextBox or PasswordBox)</param>
        public static void MarkRecommendedFieldInvalid(Control field)
        {
            StyleField(field, Brushes.Goldenrod, Brushes.Wheat);
        }

       /// <summary>
        /// 
        /// </summary>
        /// <param name="field">Field to style</param>
        /// <param name="borderColor">Field border color</param>
        /// <param name="backgroundColor">Field background color</param>
        private static void StyleField(Control field, SolidColorBrush borderColor, SolidColorBrush backgroundColor)
        {
            int BorderThickness = 2;
            Border FieldBorder = (Border) field.Parent;
            
            // Apply field margins to border (experimental)
            Thickness FieldMargins = field.Margin;
            field.Margin = new Thickness(0);

            FieldBorder.Margin = new Thickness(
                FieldMargins.Left - BorderThickness,
                FieldMargins.Top - BorderThickness,
                FieldMargins.Right - BorderThickness,
                FieldMargins.Bottom - BorderThickness
            ); 

            // Set field background color
            field.Background = backgroundColor;

            // Set border color and thickness
            FieldBorder.BorderBrush = borderColor;
            FieldBorder.BorderThickness = new Thickness(BorderThickness);
 
        }
    }
}
