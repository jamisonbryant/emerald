﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace Emerald
{
    public class TimeHelper
    {
        /// <summary>
        /// Is the application in Demo Mode?
        /// </summary>
        private readonly bool demoMode = ConfigurationManager.AppSettings["DemoMode"].Equals("enabled");

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentTime()
        {
            var setting = ConfigComponent.GetSetting("TimeFormat");
            return DateTime.Now.ToString(ConfigComponent.GetSetting("TimeFormat"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentDate()
        {
            var setting = ConfigComponent.GetSetting("DateFormat");
            return DateTime.Now.ToString(ConfigComponent.GetSetting("DateFormat"));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static string GetCurrentTimestamp()
        {
            var setting = ConfigComponent.GetSetting("TimestampFormat");
            return DateTime.Now.ToString(ConfigComponent.GetSetting("TimestampFormat"));
        }
    }
}