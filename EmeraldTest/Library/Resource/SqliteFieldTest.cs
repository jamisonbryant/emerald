﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Emerald;

namespace EmeraldTest
{
    [TestClass]
    public class SqliteFieldTest
    {
        [TestMethod]
        public void TestIntegerFieldToSqlString()
        {
            // Create database field object
            SqliteField field = new SqliteField(
                "test_field",
                SqliteField.FieldType.INTEGER,
                true,
                false,
                false,
                false,
                0
            );

            // Get field definition SQL string
            string str = field.ToSqlString();

            // Compare field definition string to expected
            Assert.AreEqual(str, "`test_field` INTEGER NOT NULL DEFAULT 0");
        }
    }
}
