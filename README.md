# Emerald

A dispatching tool designed for the Montgomery County Community Emergency Response Team (CERT).

## Demo Mode

To enable Demo Mode, update `App.config` and change `DemoMode` to `enabled`.  While in Demo Mode, Emerald will make up fake data to stand in for actual volunteer/user data for testing and demonstration purposes. No dispatch messages will be sent or received while in this mode as well.
